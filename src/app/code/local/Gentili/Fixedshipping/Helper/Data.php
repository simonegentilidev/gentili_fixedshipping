<?php

/**
 * Gentili Fixed Shipping
 */

/**
 * Data Helper
 *
 * Default module helper
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.1.0
 * @package Shipping
 * @license GNU version 3
 */

class Gentili_Fixedshipping_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * getConfigData
     *
     * Returns the value for a given configuration.
     * @param $data
     * @return mixed
     */
    public function getConfigData($data)
    {
        return Mage::getStoreConfig('carriers/gentili_fixedshipping/' . $data);
    }
}