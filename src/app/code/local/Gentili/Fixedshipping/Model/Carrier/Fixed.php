<?php

/**
 * Gentili Fixed Shipping
 */

/**
 * Data Helper
 *
 * Default module helper
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.1.0
 * @package Shipping
 * @license GNU version 3
 */
class Gentili_Fixedshipping_Model_Carrier_Fixed extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface
{

    /**
     * $_code
     *
     * Codice univoco per metodo invio.
     * Utilizziamo il nostro modulo.
     * @var string
     */
    protected $_code = 'gentili_fixedshipping';

    /**
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return bool|Mage_Shipping_Model_Rate_Result|null
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        // instantiate method object
        $carrierTitle = Mage::helper('gentili_fixedshipping')->getConfigData('title');
        $methodTitle = Mage::helper('gentili_fixedshipping')->getConfigData('method_title');
        $methodPrice = Mage::helper('gentili_fixedshipping')->getConfigData('method_price');
        try{
            $method = Mage::getModel('shipping/rate_result_method'); // modello di magento
            $method->setCarrier($this->_code);
            $method->setCarrierTitle($carrierTitle);
            $method->setMethod('fixed');
            $method->setMethodTitle($methodTitle);
            $method->setCost($methodPrice);
            $method->setPrice($methodPrice);

        } catch (Exception $e){
            Mage::logException($e);
            return false;
        }

        //instantiate result object
        try{
            $result = Mage::getModel('shipping/rate_result');
            $result->append($method);
        }catch (Exception $e){
            Mage::logException($e);
            return false;
        }

        return $result;
    }

    /**
     * getAllowedMethods
     * @return array|void
     */
    public function getAllowedMethods()
    {
        // TODO to implements
    }

    /**
     * isTrackingAvailable
     * @return bool
     */
    public function isTrackingAvailable()
    {
        return false;

    }

}